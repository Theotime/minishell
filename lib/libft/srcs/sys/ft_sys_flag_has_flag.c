/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sys_flag_has_flag.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/21 16:10:31 by triviere          #+#    #+#             */
/*   Updated: 2015/12/21 16:10:33 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_sys_flag_has_sflag(char flag)
{
	t_sys	*sys;

	sys = ft_get_sys();
	return (sys->sflag_active(flag) != -1);
}

int		ft_sys_flag_has_lflag(char *flag)
{
	t_sys	*sys;

	sys = ft_get_sys();
	return (sys->lflag_active(flag) != -1);
}
