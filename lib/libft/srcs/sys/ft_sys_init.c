/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sys_init.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: revers <revers@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/21 16:10:41 by triviere          #+#    #+#             */
/*   Updated: 2016/01/31 12:29:51 by revers           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		ft_sys_init(int ac, char **av, size_t pgm_size)
{
	t_sys		*sys;
	int			i;

	i = 1;
	sys = ft_get_sys();
	sys->pgm = malloc(pgm_size);
	ft_bzero(sys->pgm, pgm_size);
	sys->name = av[1];
	sys->av = ft_lst_init();
	while (i < ac)
	{
		ft_lst_push(sys->av, ft_el_init((void*)av[i]));
		++i;
	}
}

void		ft_sys_init_fn(t_sys *sys)
{
	sys->init = &ft_sys_init;
	sys->add_flag = &ft_sys_add_flag;
	sys->check_flags = &ft_sys_check_flags;
	sys->has_sflag = &ft_sys_flag_has_sflag;
	sys->has_lflag = &ft_sys_flag_has_lflag;
	sys->sflag_active = &ft_sys_flag_sflag_active;
	sys->lflag_active = &ft_sys_flag_lflag_active;
	sys->active_sflag = &ft_sys_flag_active_sflag;
	sys->active_lflag = &ft_sys_flag_active_lflag;
	sys->bad_flag = &ft_sys_bag_flag;
	sys->debug = &ft_sys_debug;
	sys->log = &ft_sys_log;
	sys->error = &ft_sys_error;
	sys->die = &ft_sys_die;
	sys->display_help = &ft_sys_display_help;
	sys->get_sflag = &ft_sys_flag_get_sflag;
	sys->get_lflag = &ft_sys_flag_get_lflag;
	sys->get_params_lflag = &ft_sys_flag_get_params_lflag;
	sys->get_params_sflag = &ft_sys_flag_get_params_sflag;
	sys->get_pgm = &ft_sys_get_pgm;
}
