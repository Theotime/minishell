/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sys_flag_get_params_flag.c                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/21 16:10:28 by triviere          #+#    #+#             */
/*   Updated: 2015/12/21 16:10:29 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_lst	*ft_sys_flag_get_params_lflag(char *flag)
{
	t_sys		*sys;
	t_el		*cur;

	sys = ft_get_sys();
	cur = sys->flags->start;
	while (cur)
	{
		if (!ft_strcmp(((t_flag*)cur->data)->lname, flag))
			return ((t_flag*)cur->data)->args;
		cur = cur->next;
	}
	return (NULL);
}

t_lst	*ft_sys_flag_get_params_sflag(char flag)
{
	t_sys		*sys;
	t_el		*cur;

	sys = ft_get_sys();
	cur = sys->flags->start;
	while (cur)
	{
		if (((t_flag*)cur->data)->sname == flag)
			return ((t_flag*)cur->data)->args;
		cur = cur->next;
	}
	return (NULL);
}
