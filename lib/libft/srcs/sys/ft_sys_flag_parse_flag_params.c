/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sys_flag_parse_flag_params.c                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: revers <revers@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/21 16:09:51 by triviere          #+#    #+#             */
/*   Updated: 2016/01/31 12:29:58 by revers           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		ft_sys_flag_get_arg_flag(t_el *av, t_flag *flag)
{
	int			i;
	t_el		*cur;
	t_el		*tmp;
	t_sys		*sys;

	i = 0;
	cur = av->next;
	sys = ft_get_sys();
	while (cur && flag->args->len < flag->max)
	{
		tmp = cur->next;
		if (ft_sys_flag_is_flag_format(cur) != 0 \
			|| !ft_strcmp(((char*)cur->data), "--"))
			break ;
		ft_lst_push(flag->args, ft_el_init((void*)(char*)cur->data));
		cur = ft_lst_remove(sys->av, cur);
		if (cur)
			free(cur);
		cur = tmp;
		++i;
	}
	if (flag->args->len >= flag->min && flag->args->len <= flag->max)
		return ;
	sys->error("Bad params for options");
	sys->display_help();
}

int			ft_sys_flag_parse_sflag_params(t_el *av, char *arg)
{
	t_sys		*sys;
	t_el		*el;
	t_flag		*flag;

	sys = ft_get_sys();
	el = sys->get_sflag(*arg);
	if (!el)
		return (0);
	flag = ((t_flag*)el->data);
	flag->active = 1;
	if (flag->max > 0 && !arg[1])
		ft_sys_flag_get_arg_flag(av, flag);
	else if (flag->min > 0 && arg[1])
	{
		sys->error("Bad params for options");
		sys->display_help();
	}
	return (1);
}

int			ft_sys_flag_parse_lflag_params(t_el *av)
{
	t_sys		*sys;
	t_el		*el;
	char		*arg;
	t_flag		*flag;

	sys = ft_get_sys();
	arg = ((char*)av->data);
	el = sys->get_lflag(arg + 2);
	if (!el)
		return (0);
	flag = ((t_flag*)el->data);
	flag->active = 1;
	if (flag->max > 0)
		ft_sys_flag_get_arg_flag(av, flag);
	return (1);
}
