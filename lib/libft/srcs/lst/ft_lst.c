/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lst.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: revers <revers@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/20 17:16:12 by triviere          #+#    #+#             */
/*   Updated: 2016/01/31 15:30:54 by revers           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/libft.h"

t_lst		*ft_lst_init(void)
{
	t_lst		*lst;

	lst = ft_memalloc(sizeof(t_lst));
	lst->start = NULL;
	lst->end = NULL;
	lst->len = 0;
	return (lst);
}

void		ft_lst_push(t_lst *lst, t_el *el)
{
	el->prev = lst->end;
	el->next = NULL;
	if (lst->end)
		lst->end->next = el;
	else
		lst->start = el;
	lst->end = el;
	++lst->len;
}

void		ft_lst_unshift(t_lst *lst, t_el *el)
{
	el->next = lst->start;
	el->prev = NULL;
	if (lst->start)
		lst->start->prev = el;
	else
		lst->end = el;
	lst->start = el;
	++lst->len;
}

t_el		*ft_lst_slice(t_lst *lst)
{
	t_el	*tmp;

	if (!lst->len)
		return (NULL);
	tmp = lst->start;
	lst->start = tmp->next;
	tmp->next = NULL;
	tmp->prev = NULL;
	if (lst->len < 2)
		lst->end = NULL;
	else
		lst->start->prev = NULL;
	--lst->len;
	return (tmp);
}

t_el		*ft_lst_pop(t_lst *lst)
{
	t_el	*tmp;

	if (!lst->len)
		return (NULL);
	tmp = lst->end;
	lst->end = tmp->prev;
	tmp->next = NULL;
	tmp->prev = NULL;
	if (lst->len < 2)
		lst->start = NULL;
	else
		lst->end->next = NULL;
	--lst->len;
	return (tmp);
}
