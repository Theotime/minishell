/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_map_file_free.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/01 16:07:34 by triviere          #+#    #+#             */
/*   Updated: 2016/02/01 16:07:35 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		ft_map_file_free(t_mapfile *file)
{
	if (file->error)
		return ;
	munmap(file->content, file->size);
	close(file->fd);
	free(file->name);
	file->size = 0;
	file->error = 1;
	free(file);
}
