/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <triviere@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/21 23:39:56 by triviere          #+#    #+#             */
/*   Updated: 2013/11/22 01:19:21 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memccpy(void *dest, void const *src, int c, size_t n)
{
	size_t	i;

	i = 0;
	while (i < n && ((char *)src)[i] != (char)c)
	{
		((char *)dest)[i] = ((char *)src)[i];
		i++;
	}
	if (i < n && ((char *)src)[i] == (char)c)
	{
		((char *)dest)[i] = ((char *)src)[i];
		return (dest + i + 1);
	}
	return (0);
}
