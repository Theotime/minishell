/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <triviere@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/02 03:07:11 by triviere          #+#    #+#             */
/*   Updated: 2016/02/01 16:06:34 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"
#include <stdio.h>

static	t_fd	*get_file_descriptor(int const fd)
{
	static	t_fd	*sys = 0;
	t_fd			*cursor;

	if (sys)
	{
		cursor = sys;
		while (cursor)
		{
			if (cursor->fd == fd)
				return (cursor);
			cursor = cursor->next;
		}
	}
	cursor = malloc(sizeof(t_fd));
	cursor->fd = fd;
	cursor->buff = NULL;
	cursor->ret = 1;
	cursor->next = sys;
	sys = cursor;
	return (cursor);
}

static	int		get_buff(t_fd *sys)
{
	char	*remain;
	char	*tmp;

	remain = ft_strnew(BUFF_SIZE);
	while (!sys->buff || (!ft_strchr(sys->buff, '\n') && sys->ret > 0))
	{
		tmp = sys->buff;
		sys->ret = read(sys->fd, remain, BUFF_SIZE);
		if (sys->ret < 0)
			return (-1);
		remain[sys->ret] = 0;
		sys->buff = ft_strjoin(tmp, remain);
		free(tmp);
	}
	free(remain);
	return (1);
}

static	int		get_line(t_fd *sys, char **line)
{
	char	*tmp;
	int		i;

	i = 0;
	if (get_buff(sys) == -1)
		return (-1);
	tmp = sys->buff;
	if (tmp)
	{
		while (tmp[i] && tmp[i] && tmp[i] != '\n')
		{
			i++;
		}
		*line = ft_strsub(tmp, 0, i);
		if (tmp[i] == '\n')
			i++;
		sys->buff = ft_strsub(tmp, i, ft_strlen(sys->buff) - i);
	}
	free(tmp);
	return (1);
}

int				get_next_line(int const fd, char **line)
{
	t_fd	*sys;

	sys = get_file_descriptor(fd);
	if (!sys || get_line(sys, line) == -1)
		return (-1);
	return (sys->ret <= 0 ? sys->ret : 1);
}
