/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_puthex.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: revers <revers@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/01 16:06:56 by triviere          #+#    #+#             */
/*   Updated: 2016/02/08 17:58:45 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_puthex(uint64_t value)
{
	char	*tab;
	char	c;

	tab = "0123456789abcdef";
	if (value)
	{
		c = tab[(value & 0xF)];
		ft_puthex(value >> 4);
		ft_putchar(c);
	}
}

void	ft_putnhex(uint64_t value, int n)
{
	char	*tab;
	char	c;

	tab = "0123456789abcdef";
	if (value)
	{
		c = tab[(value & 0xF)];
		ft_putnhex(value >> 4, --n);
		ft_putchar(c);
	}
	else if (n > 0)
	{
		ft_putchar('0');
		ft_putnhex(0, --n);
	}
}
