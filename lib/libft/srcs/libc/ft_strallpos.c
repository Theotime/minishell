/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strallpos.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <triviere@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/30 00:14:57 by triviere          #+#    #+#             */
/*   Updated: 2013/11/30 01:20:57 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static	void	ft_add_str_pos(t_ints *r, int n)
{
	t_ints	result;
	int		i;

	i = 0;
	result.v = malloc(sizeof(int) * r->size + 1);
	result.size = r->size + 1;
	while (i < r->size)
	{
		result.v[i] = r->v[i];
		i++;
	}
	result.v[i] = n;
	*r = result;
}

t_ints			ft_strallpos(char const *s, char c)
{
	t_ints	result;
	int		i;

	i = 0;
	result.size = 0;
	while (s[i])
	{
		if (s[i] == c)
			ft_add_str_pos(&result, i);
		i++;
	}
	return (result);
}
