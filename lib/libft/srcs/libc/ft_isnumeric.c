/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isnumeric.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/15 16:38:58 by triviere          #+#    #+#             */
/*   Updated: 2015/12/21 16:18:00 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int			ft_isnumeric(char *nbr)
{
	int		i;

	i = nbr[0] == '+' || nbr[0] == '-' ? 1 : 0;
	if ((int)ft_strlen(nbr) <= i)
		return (0);
	while (nbr[i])
	{
		if (!ft_isdigit(nbr[i++]))
			return (0);
	}
	return (1);
}
