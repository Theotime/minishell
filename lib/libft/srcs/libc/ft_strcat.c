/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcat.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <triviere@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 14:48:44 by triviere          #+#    #+#             */
/*   Updated: 2013/12/04 05:00:47 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strcat(char *s1, char const *s2)
{
	int		i;
	int		len;
	char	*tmp;

	i = 0;
	len = ft_strlen(s1);
	tmp = ft_strdup(s1);
	s1 = ft_memrealloc((void *)s1, len + ft_strlen(s2));
	if (s1)
	{
		while (i < (int)(len + ft_strlen(s2)))
		{
			if (i < len)
				s1[i] = ((char *)tmp)[i];
			else
				s1[i] = ((char *)s2)[i - len];
			i++;
		}
		s1[i] = '\0';
	}
	return (s1);
}
