/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: revers <revers@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/25 11:11:23 by triviere          #+#    #+#             */
/*   Updated: 2015/12/20 16:21:18 by revers           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strstr(char const *s1, char const *s2)
{
	int		i;

	if (!s2)
		return ((char *)s1);
	i = 0;
	while (*s1)
	{
		if (((char *)s2)[i] == '\0')
			return ((char *)s1 - i);
		else if (*s1 == s2[i])
			i++;
		else if (i > 0)
		{
			s1 -= i;
			i = 0;
		}
		s1++;
	}
	return ((*s1 == s2[i] && !s2[i]) ? (char *)(s1 - i) : 0);
}
