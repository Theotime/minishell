/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_el.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: revers <revers@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/21 16:19:07 by triviere          #+#    #+#             */
/*   Updated: 2016/01/31 13:14:06 by revers           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_el		*ft_el_init(void *data)
{
	t_el	*el;

	el = ft_memalloc(sizeof(t_el));
	el->data = data;
	el->next = NULL;
	el->prev = NULL;
	return (el);
}
