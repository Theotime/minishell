/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnjoin.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: revers <revers@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/20 17:16:12 by triviere          #+#    #+#             */
/*   Updated: 2015/12/23 11:31:48 by revers           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strnjoin(char *str1, char *str2, int start, int len)
{
	char		*res;
	char		*tmp;

	tmp = ft_strsub(str2, start, len);
	res = ft_strjoin(str1, tmp);
	free(tmp);
	tmp = str1;
	free(str1);
	return (res);
}
