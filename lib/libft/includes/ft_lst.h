/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lst.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: revers <revers@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/21 16:06:24 by triviere          #+#    #+#             */
/*   Updated: 2016/01/31 15:07:36 by revers           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LST_H
# define FT_LST_H

# include "libft.h"

typedef struct s_lst	t_lst;
typedef struct s_el		t_el;

struct					s_el
{
	struct s_el			*next;
	struct s_el			*prev;
	void				*data;
};

struct					s_lst
{
	t_el				*start;
	t_el				*end;
	int					len;
};

t_lst					*ft_lst_init(void);
void					ft_lst_push(t_lst *lst, t_el *el);
void					ft_lst_unshift(t_lst *lst, t_el *el);
t_el					*ft_lst_slice(t_lst *lst);
t_el					*ft_lst_pop(t_lst *lst);
void					ft_lst_swipe_back_to_front(t_lst *lst);
void					ft_lst_swipe_front_to_back(t_lst *lst);
void					ft_lst_swipe(t_lst *lst, t_el *a, t_el *b);
t_el					*ft_lst_remove(t_lst *lst, t_el *el);

t_el					*ft_el_init(void *data);

#endif
