CC				= gcc
FLAGS			= -Wall -Werror -Wextra -g3
SRC_DIR			= src
OBJ_DIR			= obj
INCLUDES		= -Ilib/libft/includes -Iinclude
LDFLAGS			= -L lib/libft -lft
CFLAGS			= $(FLAGS) $(INCLUDES)

OBJS			= $(patsubst $(SRC_DIR)/%,$(OBJ_DIR)/%, $(SRCS:.c=.o))
SRCS			= $(shell find $(SRC_DIR) -type f)
NAME			= minishell

all: lib_all $(NAME)
	@echo > /dev/null

re : fclean all

clean: lib_clean
	@rm -rf obj

fclean: lib_fclean clean
	@rm -rf ${NAME}

$(NAME): $(OBJS)
	$(CC) -o $@ $^ $(LDFLAGS)

$(OBJ_DIR)/%.o : $(SRC_DIR)/%.c
	@mkdir -p $(OBJ_DIR)
	@mkdir -p $(dir $@)
	$(CC) -o $@ -c $< $(CFLAGS)

lib_all:
	@make -C lib/libft

lib_clean:
	@make clean -C lib/libft

lib_fclean:
	@make fclean -C lib/libft

.PHONY: clean fclean re all
