/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bins.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/08 17:55:33 by triviere          #+#    #+#             */
/*   Updated: 2016/02/10 15:38:48 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static int		ft_sh_has_bin(char *path)
{
	struct stat		buff;

	if (stat(path, &buff) != -1 && S_ISREG(buff.st_mode))
	{
		if (!access(path, X_OK))
			return (1);
		return (-1);
	}
	return (0);
}

static char		*join_path(char *path, char *bin)
{
	char		*tmp;
	char		*tmp2;

	tmp = ft_strjoin(path, "/");
	tmp2 = tmp;
	tmp = ft_strjoin(tmp, bin);
	free(tmp2);
	return (tmp);
}

static char		*ft_exec_local(t_sh *sh, char *bin, int *ret)
{
	char		*tmp;

	if (bin && (bin[0] == '/' || (bin[0] == '.' && bin[1] == '/')))
	{
		bin += (bin[0] != '/' ? 2 : 0);
		if (bin[0] != '/')
			tmp = join_path(sh->pwd, bin);
		else
			tmp = ft_strdup(bin);
		*ret = ft_sh_has_bin(tmp);
		if (*ret == 1)
			return (tmp);
		else if (*ret == -1)
		{
			ft_putstr(bin);
			ft_putendl(": permission denied");
		}
	}
	return (NULL);
}

static char		*ft_exec_path(t_el *el, char *bin, int *ret)
{
	char	*tmp;
	int		i;
	char	**path;

	i = 0;
	path = ft_strsplit(((t_env*)el->data)->value, ':');
	while (path[i])
	{
		tmp = join_path(path[i], bin);
		*ret = ft_sh_has_bin(tmp);
		if (*ret == 1)
			return (tmp);
		else if (*ret == -1)
		{
			ft_putstr(bin);
			ft_putendl(": permission denied");
			break ;
		}
		++i;
	}
	return (NULL);
}

char			*ft_sh_find_bin(t_sh *sh, t_el *el, char *bin)
{
	char	*tmp;
	int		ret;

	ret = 0;
	tmp = ft_exec_local(sh, bin, &ret);
	if (tmp)
		return (tmp);
	else if (tmp == NULL && ret == 0 && el != NULL)
	{
		tmp = ft_exec_path(el, bin, &ret);
		if (tmp)
			return (tmp);
	}
	if (ret == 0)
	{
		ft_putstr(bin);
		ft_putendl(": command not found");
	}
	return (NULL);
}
