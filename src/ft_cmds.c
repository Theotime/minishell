/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cmds.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/08 17:55:37 by triviere          #+#    #+#             */
/*   Updated: 2016/02/08 17:55:38 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

t_cmd			*ft_cmd_init(const char *name, int (*fn)(char **av))
{
	t_cmd		*cmd;

	cmd = ft_memalloc(sizeof(t_cmd));
	cmd->name = ft_strdup(name);
	cmd->fn = fn;
	return (cmd);
}
