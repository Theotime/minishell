/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dispatch.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/08 17:55:40 by triviere          #+#    #+#             */
/*   Updated: 2016/02/19 16:29:19 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include <stdio.h>

static int		ft_sh_is_builtin(char *str, char **av)
{
	t_sh		*sh;
	t_el		*cur;
	t_cmd		*cmd;

	sh = ft_get_sys()->get_pgm();
	cur = sh->cmds->start;
	while (cur)
	{
		cmd = (t_cmd*)cur->data;
		if (ft_strcmp(cmd->name, str) == 0)
		{
			if (av)
				cmd->fn(av + 1);
			else
				cmd->fn(av);
			return (1);
		}
		cur = cur->next;
	}
	return (0);
}

static int		ft_cmd_valid_format(char *str)
{
	int		n;
	int		m;
	int		o;
	int		i;

	n = 0;
	i = 0;
	o = 0;
	m = 0;
	while (str[i])
	{
		if (str[i] == '"' && o % 2 == 0 && m % 2 == 0)
			++n;
		else if (str[i] == '\'' && o % 2 == 0 && n % 2 == 0)
			++m;
		else if (str[i] == '\\')
			++o;
		if (str[i] == '\'' || str[i] == '"')
			o = 0;
		++i;
	}
	return (n % 2 == 0 && m % 2 == 0);
}

static char		**ft_extract_cmd_arg(char *str, char **cmd)
{
	char	**av;

	if (!ft_cmd_valid_format(str))
	{
		*cmd = NULL;
		return (NULL);
	}
	av = NULL;
	av = ft_sh_arg_split(str);
	if (av)
		*cmd = ft_strdup(av[0]);
	return (av);
}

static int		ft_sh_is_bin(t_sh *sh, char *cmd, char **av, char env)
{
	pid_t		pid;
	int			state;
	int			res;
	char		*bin;

	bin = sh->find_bin(sh, sh->find_env("PATH"), cmd);
	if (!bin)
		return (0);
	pid = fork();
	sh->child = 1;
	res = 0;
	if (pid == 0 && env)
		res = execve(bin, av, sh->env_to_array());
	else if (pid == 0 && !env)
		res = execve(bin, av, NULL);
	else
		wait(&state);
	sh->child = 0;
	if (res == -1)
	{
		ft_putendl("minishell: exec format error");
		exit(1);
	}
	return (1);
}

void			ft_sh_dispatch(char *str, char env)
{
	t_sh		*sh;
	char		*cmd;
	char		**av;

	av = ft_extract_cmd_arg(str, &cmd);
	sh = ft_get_sys()->get_pgm();
	if (!cmd || !ft_strcmp(cmd, ""))
		return ;
	else
		ft_sh_is_builtin(cmd, av) || ft_sh_is_bin(sh, cmd, av, env);
}
