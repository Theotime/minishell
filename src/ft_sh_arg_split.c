/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sh_arg_split.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/10 14:19:20 by triviere          #+#    #+#             */
/*   Updated: 2016/02/10 15:52:49 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static int		ft_sh_get_len_arg(char *str, char ref)
{
	int		i;
	int		back;

	i = ref == '\'' || ref == '"' ? 1 : 0;
	back = 0;
	while (str[i])
	{
		if (str[i] == '\\')
			++back;
		else if ((str[i] == ref || str[i] == '\t') && back % 2 == 0)
			break ;
		else
			back = 0;
		++i;
	}
	return (i);
}

static char		*ft_sh_get_next_arg(char **str)
{
	char	ref;
	char	*tmp;
	char	*tmp2;
	int		len;
	int		n;

	if (!str || !*str || !**str)
		return (NULL);
	ref = ((*str)[0] == '\'' || (*str)[0] == '"') ? (*str)[0] : ' ';
	n = (ref == '\'' || ref == '"');
	len = ft_sh_get_len_arg(*str, ref);
	tmp = ft_strsub(*str, n, len - n);
	if (!n)
	{
		tmp2 = tmp;
		tmp = ft_strtrim(tmp);
		free(tmp2);
	}
	if ((*str)[len] && (*str)[len + 1] && (*str)[len + 1] != ' ')
		n = 0;
	if ((int)ft_strlen(*str) > len + 1 + n)
		*str = *str + len + 1 + n;
	else
		*str = NULL;
	return (tmp);
}

char			**ft_sh_arg_split(char *str)
{
	t_lst	*args;
	char	*arg;

	args = ft_lst_init();
	while ((arg = ft_sh_get_next_arg(&str)))
	{
		if (ft_strcmp(arg, ""))
			ft_lst_push(args, ft_el_init((void*)arg));
	}
	if (args->len > 0)
		return (ft_sh_lst_to_array(args));
	return (NULL);
}
