/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_env.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/08 17:55:44 by triviere          #+#    #+#             */
/*   Updated: 2016/02/10 15:57:12 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

t_el		*ft_sh_find_env(char *key)
{
	t_sh		*sh;
	t_el		*cur;
	t_env		*env;

	sh = ft_get_sys()->get_pgm();
	cur = sh->env->start;
	while (cur)
	{
		env = (t_env*)cur->data;
		if (!ft_strcmp(env->key, key))
			return (cur);
		cur = cur->next;
	}
	return (NULL);
}

t_env		*ft_env_init(char *key, char *value)
{
	t_env		*env;

	env = ft_memalloc(sizeof(t_env));
	if (!env)
		return (NULL);
	env->key = ft_strdup(key);
	if (value)
		env->value = ft_strdup(value);
	else
		env->value = NULL;
	return (env);
}

char		**ft_sh_env_to_array(void)
{
	t_sh		*sh;
	char		**env;
	char		*tmp;
	int			i;
	t_el		*cur;

	sh = ft_get_sys()->get_pgm();
	env = ft_memalloc(sizeof(char*) * sh->env->len + 1);
	cur = sh->env->start;
	i = 0;
	while (cur)
	{
		tmp = ft_strjoin(((t_env*)cur->data)->key, "=");
		env[i++] = ft_strjoin(tmp, ((t_env*)cur->data)->value);
		free(tmp);
		cur = cur->next;
	}
	return (env);
}

void		ft_sh_init_env(t_sh *sh, char **environ)
{
	char		*key;
	char		*value;
	int			pos;
	t_env		*env;

	while (*environ != NULL)
	{
		if ((pos = ft_strpos(*environ, '=')) != -1)
		{
			key = ft_strsub(*environ, 0, pos);
			++pos;
			value = ft_strsub(*environ + pos, 0, ft_strlen(*environ + pos));
			if (!ft_strcmp("SHLVL", key))
				value = ft_itoa(ft_atoi(value) + 1);
			env = ft_env_init(key, value);
			if (env != NULL)
				ft_lst_push(sh->env, ft_el_init((void*)env));
			free(value);
			free(key);
		}
		++environ;
	}
}
