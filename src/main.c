/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/08 17:56:02 by triviere          #+#    #+#             */
/*   Updated: 2016/02/19 16:03:36 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static void		int_event(int e)
{
	t_sh		*sh;

	(void)e;
	sh = ft_get_sys()->get_pgm();
	ft_putchar('\n');
	if (!sh->child)
		sh->display_prompt();
}

int				main(int ac, char **av, char **env)
{
	t_sys		*sys;
	t_sh		*sh;
	char		*cmd;
	char		*tmp;

	signal(SIGINT, int_event);
	sys = ft_get_sys();
	sys->init(ac, av, sizeof(t_sh));
	ft_sh_init(env);
	sh = sys->get_pgm();
	sh->display_prompt();
	while (get_next_line(0, &cmd) == 1)
	{
		tmp = cmd;
		cmd = ft_strtrim(cmd);
		free(tmp);
		sh->dispatch(cmd, 1);
		sh->display_prompt();
	}
	ft_putchar('\n');
	return (0);
}
