/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cmd_cd_access.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/10 14:18:36 by triviere          #+#    #+#             */
/*   Updated: 2016/02/10 14:19:09 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int			ft_cd_access(char *path)
{
	struct stat		buff;

	if (stat(path, &buff) != -1)
	{
		if (!S_ISDIR(buff.st_mode))
			return (2);
		if (!access(path, R_OK))
			return (1);
		return (-1);
	}
	return (0);
}
