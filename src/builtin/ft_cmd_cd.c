/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cmd_cd.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/08 17:54:59 by triviere          #+#    #+#             */
/*   Updated: 2016/02/15 13:22:03 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static void		ft_change_path_to_home(void)
{
	t_sh		*sh;
	t_el		*el;
	char		*tmp;

	sh = ft_get_sys()->get_pgm();
	el = sh->find_env("HOME");
	if (!el || !el->data)
		return ;
	tmp = sh->pwd;
	sh->pwd = ft_strdup(((t_env*)el->data)->value);
	free(tmp);
}

static void		ft_replace_path(char *pattern, char *replace)
{
	t_sh		*sh;
	char		*path;
	char		*tmp;
	size_t		len;
	int			i;

	sh = ft_get_sys()->get_pgm();
	path = sh->pwd;
	len = ft_strlen(pattern);
	i = 0;
	while (*path)
	{
		if (!ft_strncmp(path, pattern, len))
		{
			tmp = ft_strsub(path + len, 0, ft_strlen(path) - len);
			sh->pwd = ft_strsub(path - i, 0, i);
			sh->pwd = ft_strjoin(sh->pwd, replace);
			sh->pwd = ft_strjoin(sh->pwd, tmp);
			return ;
		}
		++path;
		++i;
	}
}

static void		ft_absolute_path(char *dest)
{
	t_sh		*sh;

	sh = ft_get_sys()->get_pgm();
	sh->pwd = ft_sh_parse_path(dest);
}

static void		ft_new_path(char **av)
{
	t_sh		*sh;

	sh = ft_get_sys()->get_pgm();
	if (!av[0])
		ft_change_path_to_home();
	else if (av[0] && av[1])
		ft_replace_path(av[0], av[1]);
	else if (av[0] && !ft_strcmp(av[0], "-") && sh->oldpwd)
		sh->pwd = sh->oldpwd;
	else if (av[0])
		ft_absolute_path(av[0]);
}

int				ft_cmd_cd(char **av)
{
	t_sh		*sh;
	char		*oldpath;
	int			ret;

	sh = ft_get_sys()->get_pgm();
	oldpath = ft_strdup(sh->pwd);
	ft_new_path(av);
	ret = ft_cd_access(sh->pwd);
	if (ret == 1)
	{
		chdir(sh->pwd);
		sh->oldpwd = ft_strdup(oldpath);
		return (0);
	}
	else if (ret == 2)
		ft_putendl("cd: is not a directory");
	else if (ret == -1)
		ft_putendl("cd: permission denied");
	else
		ft_putendl("cd: no surch file or directory");
	free(sh->pwd);
	sh->pwd = ft_strdup(oldpath);
	free(oldpath);
	return (1);
}
