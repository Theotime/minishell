/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cmd_setenv.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/08 17:55:27 by triviere          #+#    #+#             */
/*   Updated: 2016/02/15 16:23:13 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static void		ft_cmd_setenv_create(char *key, char *value)
{
	t_sh		*sh;
	t_env		*env;

	sh = ft_get_sys()->get_pgm();
	env = ft_env_init(key, value);
	ft_lst_push(sh->env, ft_el_init((void*)env));
}

static void		ft_cmd_setenv_edit(t_env *env, char *value)
{
	char		*tmp;

	tmp = env->value;
	if (value)
		env->value = ft_strdup(value);
	else
		env->value = ft_strdup("");
	free(tmp);
}

int				ft_cmd_setenv(char **av)
{
	t_sh		*sh;
	t_el		*env;

	if (!av || !av[0])
	{
		ft_putendl("usage : setenv <key> <value>");
		return (1);
	}
	sh = ft_get_sys()->get_pgm();
	env = sh->find_env(av[0]);
	if (env)
		ft_cmd_setenv_edit((t_env*)env->data, av[1]);
	else
		ft_cmd_setenv_create(av[0], av[1]);
	return (0);
}
