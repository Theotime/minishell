/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cmd_env.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/08 17:55:14 by triviere          #+#    #+#             */
/*   Updated: 2016/02/08 17:55:19 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static void		ft_sh_env_display(void)
{
	t_el	*cur;
	t_env	*env;

	cur = ((t_sh*)ft_get_sys()->get_pgm())->env->start;
	while (cur)
	{
		env = (t_env*)cur->data;
		ft_putstr(env->key);
		ft_putchar('=');
		ft_putendl(env->value);
		cur = cur->next;
	}
}

static char		*ft_cmd_join_arg(char **av)
{
	int		i;
	char	*tmp;
	char	*result;

	i = 1;
	if (!av || !av[0])
		return (ft_strdup(""));
	result = ft_strdup(av[0]);
	while (av[i])
	{
		tmp = result;
		result = ft_strjoin(result, " ");
		free(tmp);
		tmp = result;
		result = ft_strjoin(result, av[i]);
		free(tmp);
		++i;
	}
	return (result);
}

static void		ft_env_display_verbose(char **av)
{
	int		i;

	i = 0;
	ft_putstr("#env executing: ");
	ft_putendl(av[0]);
	while (av[i])
	{
		ft_putstr("#env    arg[");
		ft_putnbr(i);
		ft_putstr("]= '");
		ft_putstr(av[i]);
		ft_putendl("'");
		++i;
	}
}

static void		ft_parse_flag_env(char ***av, char *i, char *v)
{
	*i = 0;
	*v = 0;
	if (*av && *av[0] && \
		(!ft_strcmp(*av[0], "-iv") || !ft_strcmp(*av[0], "-vi")))
	{
		*i = 1;
		*v = 1;
	}
	else if (*av && *av[0] && !ft_strcmp(*av[0], "-v"))
		*v = 1;
	else if (*av && *av[0] && !ft_strcmp(*av[0], "-i"))
		*i = 1;
	if (*i || *v)
		++*av;
}

int				ft_cmd_env(char **av)
{
	t_sh		*sh;
	char		*tmp;
	char		i;
	char		v;

	sh = ft_get_sys()->get_pgm();
	ft_parse_flag_env(&av, &i, &v);
	tmp = ft_cmd_join_arg(av);
	if (i && v)
		ft_putendl("#env clearing environ");
	if ((!av || !av[0]) && !i)
		ft_sh_env_display();
	else if (av && av[0] && v)
		ft_env_display_verbose(av);
	if (av && av[0] && i && ft_strcmp(av[0], "env"))
		sh->dispatch(tmp, 0);
	else if (av && av[0] && !i)
		sh->dispatch(tmp, 1);
	return (0);
}
