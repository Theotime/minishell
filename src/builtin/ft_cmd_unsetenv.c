/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cmd_unsetenv.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/08 17:55:30 by triviere          #+#    #+#             */
/*   Updated: 2016/02/08 17:55:31 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static void		ft_cmd_env_remove(t_lst *env, t_el *cur)
{
	if (!cur)
	{
		ft_putendl("Not found");
		return ;
	}
	ft_lst_remove(env, cur);
}

int				ft_cmd_unsetenv(char **av)
{
	t_sh		*sh;
	t_el		*env;
	int			i;

	if (!av || !av[0])
	{
		ft_putendl("usage : unsetenv <key1> <key2> <keyN>");
		return (1);
	}
	i = 0;
	sh = ft_get_sys()->get_pgm();
	while (av[i])
	{
		env = sh->find_env(av[i]);
		ft_cmd_env_remove(sh->env, env);
		++i;
	}
	return (0);
}
