/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lst_to_array.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/10 14:18:51 by triviere          #+#    #+#             */
/*   Updated: 2016/02/10 14:18:53 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

char		**ft_sh_lst_to_array(t_lst *array)
{
	char	**strs;
	int		i;
	t_el	*cur;

	strs = ft_memalloc(sizeof(char*) * (array->len + 1));
	cur = array->start;
	i = 0;
	while (cur)
	{
		strs[i] = ft_strdup((char*)cur->data);
		cur = cur->next;
		++i;
	}
	return (strs);
}
