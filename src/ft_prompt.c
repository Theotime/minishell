/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_prompt.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/08 17:55:52 by triviere          #+#    #+#             */
/*   Updated: 2016/02/08 17:55:54 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void			ft_sh_display_prompt(void)
{
	t_sh		*sh;

	sh = ft_get_sys()->get_pgm();
	ft_putstr("$ ");
	ft_putstr(sh->pwd);
	ft_putstr(" > ");
}
