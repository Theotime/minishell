/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_home.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/08 17:55:46 by triviere          #+#    #+#             */
/*   Updated: 2016/02/08 17:57:08 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

char		*ft_sh_get_home(void)
{
	t_sh		*sh;
	t_el		*el;

	sh = ft_get_sys()->get_pgm();
	el = sh->find_env("HOME");
	if (el)
		return (((t_env*)el->data)->value);
	return (NULL);
}
