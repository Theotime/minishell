/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_path.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/08 17:55:49 by triviere          #+#    #+#             */
/*   Updated: 2016/02/08 17:58:08 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static char		*ft_sh_lst_path_to_char(t_lst *path)
{
	char		*tmp;
	char		*tmp2;
	char		*dest;
	t_el		*cur;

	cur = path->start;
	dest = NULL;
	while (cur)
	{
		tmp = ft_strjoin("/", (char*)cur->data);
		if (dest)
		{
			tmp2 = dest;
			dest = ft_strjoin(dest, tmp);
			free(tmp2);
		}
		else
			dest = ft_strdup(tmp);
		free(tmp);
		cur = cur->next;
	}
	if (dest)
		return (dest);
	else
		return (ft_strdup("/"));
}

static char		*ft_sh_parse_absolute_path(char *path)
{
	int			i;
	char		**tmp;
	t_lst		*folders;
	t_el		*el;

	tmp = ft_strsplit(path, '/');
	folders = ft_lst_init();
	i = 0;
	while (tmp[i])
	{
		if (ft_strcmp(tmp[i], "..") == 0)
		{
			el = ft_lst_pop(folders);
			if (el)
				free(el);
		}
		else if (ft_strcmp(tmp[i], ".") != 0)
			ft_lst_push(folders, ft_el_init((void*)tmp[i]));
		++i;
	}
	return (ft_sh_lst_path_to_char(folders));
}

char			*ft_sh_parse_path(char *path)
{
	t_sh		*sh;
	char		*tmp;
	char		*dest;

	sh = ft_get_sys()->get_pgm();
	if (path[0] == '/')
		return (ft_sh_parse_absolute_path(path));
	else if (path[0] == '~')
	{
		if (!sh->get_home())
			return (NULL);
		tmp = ft_strjoin(sh->get_home(), "/");
		dest = ft_strjoin(tmp, path + 1);
	}
	else
	{
		tmp = ft_strjoin(sh->pwd, "/");
		dest = ft_strjoin(tmp, path);
	}
	free(tmp);
	return (ft_sh_parse_absolute_path(dest));
}
