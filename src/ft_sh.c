/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sh.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/08 17:55:57 by triviere          #+#    #+#             */
/*   Updated: 2016/02/08 17:57:32 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static void		ft_sh_init_fn(t_sh *sh)
{
	sh->display_prompt = &ft_sh_display_prompt;
	sh->dispatch = &ft_sh_dispatch;
	sh->find_env = &ft_sh_find_env;
	sh->find_bin = &ft_sh_find_bin;
	sh->env_to_array = &ft_sh_env_to_array;
	sh->get_home = &ft_sh_get_home;
}

static void		ft_sh_init_cmds(t_sh *sh)
{
	t_cmd		*cmd;

	cmd = ft_cmd_init("env", ft_cmd_env);
	ft_lst_push(sh->cmds, ft_el_init((void*)cmd));
	cmd = ft_cmd_init("exit", ft_cmd_exit);
	ft_lst_push(sh->cmds, ft_el_init((void*)cmd));
	cmd = ft_cmd_init("setenv", ft_cmd_setenv);
	ft_lst_push(sh->cmds, ft_el_init((void*)cmd));
	cmd = ft_cmd_init("unsetenv", ft_cmd_unsetenv);
	ft_lst_push(sh->cmds, ft_el_init((void*)cmd));
	cmd = ft_cmd_init("cd", ft_cmd_cd);
	ft_lst_push(sh->cmds, ft_el_init((void*)cmd));
	cmd = ft_cmd_init("pwd", ft_cmd_pwd);
	ft_lst_push(sh->cmds, ft_el_init((void*)cmd));
}

void			ft_sh_init(char **env)
{
	t_sys		*sys;
	t_sh		*sh;

	sys = ft_get_sys();
	sh = sys->get_pgm();
	sh->cmds = ft_lst_init();
	sh->env = ft_lst_init();
	sh->pwd = getcwd(NULL, -1);
	sh->child = 0;
	sh->oldpwd = NULL;
	ft_sh_init_fn(sh);
	ft_sh_init_cmds(sh);
	ft_sh_init_env(sh, env);
}
