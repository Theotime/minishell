/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: triviere <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/08 17:56:25 by triviere          #+#    #+#             */
/*   Updated: 2016/02/10 14:23:19 by triviere         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINISHELL_H
# define MINISHELL_H

# include "libft.h"
# include <dirent.h>
# include <signal.h>
# include <sys/types.h>
# include <sys/stat.h>

typedef struct s_sh		t_sh;
typedef struct s_cmd	t_cmd;
typedef struct s_env	t_env;
typedef struct s_bin	t_bin;

struct					s_sh
{
	t_lst				*env;
	t_lst				*cmds;

	char				*pwd;
	char				*oldpwd;
	char				child;

	void				(*display_prompt)(void);
	void				(*dispatch)(char *cmd, char env);
	t_el				*(*find_env)(char *key);
	char				*(*find_bin)(t_sh *sh, t_el *env, char *cmd);
	char				**(*env_to_array)(void);
	char				*(*get_home)(void);
};

struct					s_cmd
{
	char				*name;
	int					(*fn)(char **av);
};

struct					s_env
{
	char				*key;
	char				*value;
};

struct					s_bin
{
	char				*path;
	char				*name;
};

int						ft_cmd_env(char **av);
int						ft_cmd_exit(char **av);
int						ft_cmd_setenv(char **av);
int						ft_cmd_unsetenv(char **av);
int						ft_cmd_cd(char **av);
int						ft_cmd_pwd(char **av);
int						ft_cd_access(char *path);

char					*ft_sh_parse_path(char *path);
char					*ft_sh_get_home(void);
char					*ft_sh_find_bin(t_sh *sh, t_el *env, char *cmd);

char					**ft_sh_lst_to_array(t_lst *array);
char					**ft_sh_env_to_array(void);
char					**ft_sh_arg_split(char *str);

void					ft_sh_dispatch(char *cmd, char env);
void					ft_sh_display_prompt(void);
void					ft_sh_init_env(t_sh *sh, char **environ);
void					ft_sh_init(char **env);

t_cmd					*ft_cmd_init(const char *name, int (*fn)(char **av));

t_env					*ft_env_init(char *key, char *value);

t_el					*ft_sh_find_env(char *key);

#endif
